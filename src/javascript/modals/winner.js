import { showModal } from "./modal";
import { createElement } from '../helpers/domHelper';

export function showWinnerModal(fighter) {
  const title = 'Winner is ' + fighter.name;
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
  animateFighter(bodyElement);
}

function createWinnerDetails(fighter) {
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const imageElement = new Image(200, 300);
  imageElement.src = fighter.source;
  fighterDetails.append(imageElement);
  return fighterDetails;
}


function animateFighter(firstFighter) {
  var pos = 0;
  let id = setInterval(animate, 15);
  setTimeout(() => clearInterval(id), 7000);
  function animate() {
    if(pos > 70){
      pos = -70;
    }
    pos++;
    firstFighter.style.left = pos + "px";
  }
}