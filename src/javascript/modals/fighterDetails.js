import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const imageElement = new Image(200,300);

  nameElement.innerText = 'Name: ' + name + '\r\n';
  attackElement.innerText = 'Attack: ' + attack + '\r\n';
  defenseElement.innerText = 'Defense: ' + defense + '\r\n';
  healthElement.innerText = 'Health: ' + health + '\r\n';
  imageElement.src = source;
  fighterDetails.append(nameElement, attackElement, defenseElement, healthElement, imageElement);
  return fighterDetails;
}
