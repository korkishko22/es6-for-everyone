export function fight(firstFighter, secondFighter) {
  let firstFigterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;
  while (firstFigterHealth > 0 && secondFighterHealth > 0) {
    secondFighterHealth -= getDamage(firstFighter, secondFighter);
    firstFigterHealth -= getDamage(secondFighter, firstFighter);
  }
  const winner = firstFigterHealth > 0 ? firstFighter : secondFighter; 
  return winner;
}

export function getDamage(attacker, enemy) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(enemy);
  return hitPower - blockPower;
}

export function getHitPower(fighter) {
  const criticalChance = getRandomNumberInRange(1, 2);
  return fighter.attack * criticalChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomNumberInRange(1, 2);
  return fighter.defense * dodgeChance;
}

function getRandomNumberInRange(min, max) {
  return Math.random() * (max - min) + min;
}